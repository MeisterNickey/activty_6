﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ReportingAPI.Models;
using System.Web.Http.Cors;
using System.Dynamic;

namespace ReportingAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class lgcustomersController : ApiController
    {
       
        private HardwareDBEntities db = new HardwareDBEntities();
        
        // GET: api/lgcustomers
        public dynamic Getlgcustomers()
        {
            db.Configuration.ProxyCreationEnabled = false;
            ////you have to always start with linq with the table that is connected to all the tables
            var result = db.lginvoices.Include(gg=> gg.lgcustomer).Include(hg=>hg.lgemployee).ToList();
            return getcustomer(result);
        }

        public dynamic getcustomer(List<lginvoice> ins)
        {
            //this is the main that will hold all the information of the report
            dynamic dynaminvoice = new ExpandoObject();
            try
            {
                //this will group the employee and count their invoices
                //
                var employee = ins.GroupBy(g => g.lgemployee.emp_fname);
                List<dynamic> employeelist = new List<dynamic>();
                foreach (var emp in employee)
                {
                    dynamic dynamemp = new ExpandoObject();

                    dynamemp.Name = emp.Key;
                    dynamemp.Totalinvoices = emp.Sum(g=>g.inv_num);
                    employeelist.Add(dynamemp);
                }
                dynaminvoice.Employee = employeelist;
                ///here is the other table where I group the information by Customer
                var customer = ins.GroupBy(g => g.lgcustomer.cust_code);
                List<dynamic> customerlist = new List<dynamic>();
                foreach (var cus in customer)
                {
                    dynamic dynamcus = new ExpandoObject();

                    dynamcus.Name = cus.Key;
                    dynamcus.Totalinvoices = cus.Sum(g => g.inv_total);
                    List<dynamic> flexi = new List<dynamic>();
                    foreach (var item in cus)
                    {
                        dynamic dynamcus1 = new ExpandoObject();
                        dynamcus1.Name = item.lgcustomer.cust_fname+ " " + item.lgcustomer.cust_lname;
                        dynamcus1.employeename = item.lgemployee.emp_fname + " " + item.lgemployee.emp_lname;
                        dynamcus1.invoicetotal = item.inv_total;
                        flexi.Add(dynamcus1);

                    }
                    dynamcus.flexi = flexi;
                    customerlist.Add(dynamcus);
                }
                dynaminvoice.Customer = customerlist;
            }
            catch (Exception e)
            {
                string error = e.Message;
                throw;
            }
            return dynaminvoice;
        }

        // GET: api/lgcustomers/5
        [ResponseType(typeof(lgcustomer))]
        public dynamic Getlgcustomer(decimal id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            dynamic cus = db.lgcustomers.Where(z=>z.cust_code == id).Include(g => g.lginvoices).FirstOrDefault();
            if (cus == null)
            {
                return NotFound();
            }

            return cus;
        }

        // PUT: api/lgcustomers/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putlgcustomer(decimal id, lgcustomer lgcustomer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != lgcustomer.cust_code)
            {
                return BadRequest();
            }

            db.Entry(lgcustomer).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!lgcustomerExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/lgcustomers
        [ResponseType(typeof(lgcustomer))]
        public IHttpActionResult Postlgcustomer(lgcustomer lgcustomer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.lgcustomers.Add(lgcustomer);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (lgcustomerExists(lgcustomer.cust_code))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = lgcustomer.cust_code }, lgcustomer);
        }

        // DELETE: api/lgcustomers/5
        [ResponseType(typeof(lgcustomer))]
        public IHttpActionResult Deletelgcustomer(decimal id)
        {
            lgcustomer lgcustomer = db.lgcustomers.Find(id);
            if (lgcustomer == null)
            {
                return NotFound();
            }

            db.lgcustomers.Remove(lgcustomer);
            db.SaveChanges();

            return Ok(lgcustomer);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool lgcustomerExists(decimal id)
        {
            return db.lgcustomers.Count(e => e.cust_code == id) > 0;
        }
    }
}